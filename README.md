# Verwendung

## Starten der Umgebung

1. dieses Repo clonen
2. in dem Clone ```docker-compose up``` ausführen
3. Auf das Webinterface gehen und Gitea Basis-Setup durchführen


## Gitea Basis Setup

Die Einrichtung vom Gitea benötigt den Hostnamen des Docker-Hosts, um die
URLs für die Repos korrekt zu generieren. Somit ist eine manuelle Einrichtung
notwendig.

Der Erste Besucher der Gitea-Webseite kann die Installationsassistenten
ausführen. Der erste Registrierte Benutzer wird automatisch Admin.


Bei Gitea Setup achten auf:

- richtige HTTP-User
- rictige SSH-Domains
- richters ssh-Port (2222)

